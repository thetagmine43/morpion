export function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];

    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}

export function calculateIa(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (
      squares[a] === "O" &&
      squares[a] === squares[b] &&
      squares[c] === null
    ) {
      return c;
    } else if (
      squares[a] === "O" &&
      squares[a] === squares[c] &&
      squares[b] === null
    ) {
      return b;
    } else if (
      squares[c] === "O" &&
      squares[c] === squares[b] &&
      squares[a] === null
    ) {
      return a;
    } else if (squares[a] && squares[a] === squares[b] && squares[c] === null) {
      return c;
    } else if (squares[a] && squares[a] === squares[c] && squares[b] === null) {
      return b;
    } else if (squares[c] && squares[c] === squares[b] && squares[a] === null) {
      return a;
    }
  }
  return null;
}

export function addToStorage(status) {
  const array = JSON.parse(localStorage.getItem("history"))
    ? JSON.parse(localStorage.getItem("history"))
    : [];
  array.push(status);
  localStorage.setItem("history", JSON.stringify(array));
}
