import {fireEvent, render, screen} from '@testing-library/react';
import App from './App';

test('click on board', () => {
  render(<App />);
  const linkElement = screen.getAllByRole('button')
  fireEvent.click(linkElement[0])
  expect(linkElement[0].innerHTML).toEqual('X');
  expect(screen.getByText(/X/g)).toBeInTheDocument();
});

test('click on reset', () => {
  render(<App />);
  const linkElement = screen.getByText('Reset')
  const board = screen.getAllByRole('button', {name:"square"})
  fireEvent.click(linkElement)
  board.forEach((item, i) => {
    expect(board[i].innerHTML).toEqual("");
  })
});
