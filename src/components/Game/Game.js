import './Game.css';
import React, {useState} from "react";
import Board from "../Board/Board";
import History from "../History/History";

function Game() {
    const $defaultBoard = {
        squares: Array(9).fill(null),
        xIsNext: true,
        noWinner: false,
    };

    const [boardState, setBoardState] = useState($defaultBoard)

    const resetArray = () => {
        setBoardState($defaultBoard);
    }

    return(
        <div className="game">
            <div className="game-board">
                <Board boardState={boardState} setBoardState={setBoardState}/>
            </div>
            <button type="button" className="reset" onClick={() => resetArray()}>Reset</button>
            <div>
                <History />
            </div>
        </div>
    );
}

export default Game;