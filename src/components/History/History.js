import "./History.css";

function History() {
  const history = JSON.parse(localStorage.getItem("history"))
    ? JSON.parse(localStorage.getItem("history"))
    : [];
  const historyArray = history.reverse();

  return (
    <div>
      <h3>Last Games History</h3>
      <table>
        <tbody>
          {historyArray.map((item, key) => (
            <tr key={key}>
              <td>{item}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default History;
