import "./Board.css";
import Square from "../Square/Square";
import React, { useEffect } from "react";
import { addToStorage, calculateWinner, calculateIa } from "../../utils";

function Board({ boardState, setBoardState }) {
  const winner = calculateWinner(boardState.squares);

  let status;
  boardState.noWinner = false;
  boardState.squares.forEach((items) => {
    if (items === null) {
      boardState.noWinner = true;
    }
  });

  if (winner) {
    status = winner + " a gagné";
    addToStorage(status);
  } else if (boardState.noWinner === true) {
    status = "Prochain joueur : " + (boardState.xIsNext ? "X" : "O");
  } else {
    status = "Game Over, aucun gagnant";
    addToStorage(status);
  }

  function random() {
    console.log("Izz");
    console.log(boardState.xIsNext);
    if (boardState.xIsNext === false) {
      const Ia = calculateIa(boardState.squares);
      console.log("Ia1");
      console.log(Ia);
      let coup = Ia;
      if (Ia) {
        console.log("Ia2");
        console.log(Ia);
        coup = Ia;
      } else {
        let notUsed = [];
        boardState.squares.forEach((items, index) => {
          if (items === null) {
            notUsed.push(index);
          }
        });
        const number = notUsed[Math.floor(Math.random() * notUsed.length)];
        coup = number;
      }
      const squares = boardState.squares.slice();
      squares[coup] = boardState.xIsNext ? "X" : "O";
      setBoardState({
        squares: squares,
        xIsNext: !boardState.xIsNext,
      });
    }
  }
  useEffect(() => {
    setTimeout(() => {
      random();
    }, 900);
  });
  function handleClick(i) {
    const squares = boardState.squares.slice();
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = boardState.xIsNext ? "X" : "O";
    setBoardState({
      squares: squares,
      xIsNext: !boardState.xIsNext,
    });
    console.log(boardState.xIsNext);
  }

  function renderSquare(i) {
    return (
      <Square value={boardState.squares[i]} onClick={() => handleClick(i)} />
    );
  }

  return (
    <div>
      <div className="status">{status}</div>
      <div className="board-row">
        {renderSquare(0)}
        {renderSquare(1)}
        {renderSquare(2)}
      </div>
      <div className="board-row">
        {renderSquare(3)}
        {renderSquare(4)}
        {renderSquare(5)}
      </div>
      <div className="board-row">
        {renderSquare(6)}
        {renderSquare(7)}
        {renderSquare(8)}
      </div>
    </div>
  );
}

export default Board;
